$(function () {
    $.ajax('http://api.dev.cakeiteasy.no/api/images/bakeries/', {
        success: function (data) {
            data.forEach(
                el => {
                    const img = document.createElement('img')
                    img.src = el.thumbnail;
                    document.querySelector('body').appendChild(img);
                }
            )
        }
    })
});
//////////////////////////////////////////////

[{
    "id": 25,
    "name": "Che Bakery",
    "email": "igorboky+che@gmail.com",
    "phone": "+47 234234234234",
    "picture": 1548,
    "country": {
        "code": "NO",
        "name": "Norway",
        "currency": "NOK",
        "currency_symbol": "kr",
        "phone_code": "47",
        "language": "no"
    },
    "pay_in_store_widget_enabled": true,
    "pay_in_store_widget_only_method": false,
    "pay_in_store_store_enabled": false,
    "pay_in_store_store_only_method": false,
    "home_page_url": "http://devhouse.pro/",
    "schedule": {
        "monday": {"order_before": 840, "day_off": false, "days_before_order": 3, "is_relative": true},
        "tuesday": {"order_before": 840, "day_off": false, "days_before_order": 1, "is_relative": true},
        "wednesday": {"order_before": 840, "day_off": false, "days_before_order": 2, "is_relative": true},
        "thursday": {"order_before": 840, "day_off": false, "days_before_order": 2, "is_relative": true},
        "friday": {"order_before": 840, "day_off": false, "days_before_order": 2, "is_relative": true},
        "saturday": {"order_before": 840, "day_off": false, "days_before_order": 1, "is_relative": true},
        "sunday": {"order_before": 840, "day_off": false, "days_before_order": 1, "is_relative": true}
    },
    "position": "MIDDLE",
    "help_bakeries_promo": "<p>- Tilbyr brød, bakst og kaker<br>- Kan bestille på nettbutikk<br>- Levering på dør for 99 kr<br>- 11 utsalg er åpne</p>"
}, {
    "id": 120,
    "name": "Test schedule",
    "email": "natalia.d@devhouse.pro",
    "phone": "",
    "picture": 3570,
    "country": {
        "code": "NO",
        "name": "Norway",
        "currency": "NOK",
        "currency_symbol": "kr",
        "phone_code": "47",
        "language": "no"
    },
    "pay_in_store_widget_enabled": true,
    "pay_in_store_widget_only_method": false,
    "pay_in_store_store_enabled": false,
    "pay_in_store_store_only_method": false,
    "home_page_url": "",
    "schedule": {
        "monday": {"order_before": 720, "day_off": false, "days_before_order": 2, "is_relative": false},
        "tuesday": {"order_before": 780, "day_off": false, "days_before_order": 1, "is_relative": true},
        "wednesday": {"order_before": 840, "day_off": false, "days_before_order": 2, "is_relative": true},
        "thursday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "friday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "saturday": {"order_before": 720, "day_off": false, "days_before_order": 3, "is_relative": true},
        "sunday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true}
    },
    "position": "MIDDLE",
    "help_bakeries_promo": "<ol><li>Lorem <i>ipsum dolor sit amet</i>, consectetur adipiscing elit, sed do eiusmod temp<i>or incididunt ut labore e</i>t dolore magna aliqua.</li><li>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</li></ol><blockquote><p><br>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat&nbsp;</p></blockquote>"
}, {
    "id": 127,
    "name": "Test Sizes",
    "email": "natalia.d@devhouse.pro",
    "phone": "",
    "picture": 5451,
    "country": {
        "code": "NO",
        "name": "Norway",
        "currency": "NOK",
        "currency_symbol": "kr",
        "phone_code": "47",
        "language": "no"
    },
    "pay_in_store_widget_enabled": false,
    "pay_in_store_widget_only_method": false,
    "pay_in_store_store_enabled": false,
    "pay_in_store_store_only_method": false,
    "home_page_url": "",
    "schedule": {
        "monday": {"order_before": 720, "day_off": false, "days_before_order": 0, "is_relative": false},
        "tuesday": {"order_before": 1200, "day_off": false, "days_before_order": 1, "is_relative": true},
        "wednesday": {"order_before": 1380, "day_off": false, "days_before_order": 0, "is_relative": true},
        "thursday": {"order_before": 1380, "day_off": false, "days_before_order": 0, "is_relative": true},
        "friday": {"order_before": 960, "day_off": false, "days_before_order": 0, "is_relative": true},
        "saturday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "sunday": {"order_before": 720, "day_off": false, "days_before_order": 2, "is_relative": true}
    },
    "position": "MIDDLE",
    "help_bakeries_promo": null
}, {
    "id": 1,
    "name": "Test bakery pay in store",
    "email": "123123asdaq@adras.ru",
    "phone": "+47 123565",
    "picture": 5635,
    "country": {
        "code": "NO",
        "name": "Norway",
        "currency": "NOK",
        "currency_symbol": "kr",
        "phone_code": "47",
        "language": "no"
    },
    "pay_in_store_widget_enabled": true,
    "pay_in_store_widget_only_method": true,
    "pay_in_store_store_enabled": true,
    "pay_in_store_store_only_method": true,
    "home_page_url": "",
    "schedule": {
        "monday": {"order_before": 900, "day_off": false, "days_before_order": 1, "is_relative": false},
        "tuesday": {"order_before": 900, "day_off": false, "days_before_order": 1, "is_relative": false},
        "wednesday": {"order_before": 900, "day_off": false, "days_before_order": 1, "is_relative": false},
        "thursday": {"order_before": 900, "day_off": false, "days_before_order": 1, "is_relative": false},
        "friday": {"order_before": 900, "day_off": false, "days_before_order": 1, "is_relative": false},
        "saturday": {"order_before": 780, "day_off": false, "days_before_order": 1, "is_relative": false},
        "sunday": {"order_before": 780, "day_off": false, "days_before_order": 1, "is_relative": false}
    },
    "position": "BOTTOM",
    "help_bakeries_promo": null
}, {
    "id": 99,
    "name": "Tor Sevaldsen",
    "email": "natalia.d@devhouse.pro",
    "phone": "51561547",
    "picture": 1685,
    "country": {
        "code": "NO",
        "name": "Norway",
        "currency": "NOK",
        "currency_symbol": "kr",
        "phone_code": "47",
        "language": "no"
    },
    "pay_in_store_widget_enabled": false,
    "pay_in_store_widget_only_method": false,
    "pay_in_store_store_enabled": false,
    "pay_in_store_store_only_method": false,
    "home_page_url": "",
    "schedule": {
        "monday": {"order_before": 720, "day_off": false, "days_before_order": 3, "is_relative": false},
        "tuesday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "wednesday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "thursday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "friday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "saturday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "sunday": {"order_before": 720, "day_off": false, "days_before_order": 2, "is_relative": false}
    },
    "position": "MIDDLE",
    "help_bakeries_promo": ""
}, {
    "id": 21,
    "name": "Another Autotest",
    "email": "natalia.d@devhouse.pro",
    "phone": "",
    "picture": 6141,
    "country": {
        "code": "NO",
        "name": "Norway",
        "currency": "NOK",
        "currency_symbol": "kr",
        "phone_code": "47",
        "language": "no"
    },
    "pay_in_store_widget_enabled": false,
    "pay_in_store_widget_only_method": false,
    "pay_in_store_store_enabled": false,
    "pay_in_store_store_only_method": false,
    "home_page_url": "",
    "schedule": {
        "monday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": false},
        "tuesday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": false},
        "wednesday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": false},
        "thursday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": false},
        "friday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": false},
        "saturday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": false},
        "sunday": {"order_before": 720, "day_off": false, "days_before_order": 2, "is_relative": false}
    },
    "position": "MIDDLE",
    "help_bakeries_promo": null
}, {
    "id": 12,
    "name": "Tor Sevaldsen2",
    "email": "Natalia.d@devhouse.pro",
    "phone": "8",
    "picture": 182,
    "country": {
        "code": "NO",
        "name": "Norway",
        "currency": "NOK",
        "currency_symbol": "kr",
        "phone_code": "47",
        "language": "no"
    },
    "pay_in_store_widget_enabled": false,
    "pay_in_store_widget_only_method": false,
    "pay_in_store_store_enabled": false,
    "pay_in_store_store_only_method": false,
    "home_page_url": "",
    "schedule": {
        "monday": {"order_before": 840, "day_off": false, "days_before_order": 3, "is_relative": false},
        "tuesday": {"order_before": 720, "day_off": false, "days_before_order": 4, "is_relative": true},
        "wednesday": {"order_before": 720, "day_off": false, "days_before_order": 2, "is_relative": true},
        "thursday": {"order_before": 720, "day_off": false, "days_before_order": 2, "is_relative": false},
        "friday": {"order_before": 720, "day_off": false, "days_before_order": 2, "is_relative": true},
        "saturday": {"order_before": 720, "day_off": false, "days_before_order": 4, "is_relative": true},
        "sunday": {"order_before": 720, "day_off": false, "days_before_order": 4, "is_relative": false}
    },
    "position": "MIDDLE",
    "help_bakeries_promo": ""
}, {
    "id": 35,
    "name": "Mama's bakery",
    "email": "natalia.d@devhouse.pro",
    "phone": "+47 458156",
    "picture": 5452,
    "country": {
        "code": "NO",
        "name": "Norway",
        "currency": "NOK",
        "currency_symbol": "kr",
        "phone_code": "47",
        "language": "no"
    },
    "pay_in_store_widget_enabled": true,
    "pay_in_store_widget_only_method": false,
    "pay_in_store_store_enabled": true,
    "pay_in_store_store_only_method": true,
    "home_page_url": "",
    "schedule": {
        "monday": {"order_before": 720, "day_off": false, "days_before_order": 3, "is_relative": false},
        "tuesday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "wednesday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "thursday": {"order_before": 840, "day_off": false, "days_before_order": 1, "is_relative": true},
        "friday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "saturday": {"order_before": 720, "day_off": false, "days_before_order": 2, "is_relative": false},
        "sunday": {"order_before": 720, "day_off": false, "days_before_order": 2, "is_relative": false}
    },
    "position": "MIDDLE",
    "help_bakeries_promo": ""
}, {
    "id": 15,
    "name": "Marius Bakeri",
    "email": "natalia.d@devhouse.pro",
    "phone": "22332600",
    "picture": 371,
    "country": {
        "code": "NO",
        "name": "Norway",
        "currency": "NOK",
        "currency_symbol": "kr",
        "phone_code": "47",
        "language": "no"
    },
    "pay_in_store_widget_enabled": false,
    "pay_in_store_widget_only_method": false,
    "pay_in_store_store_enabled": false,
    "pay_in_store_store_only_method": false,
    "home_page_url": "",
    "schedule": {
        "monday": {"order_before": 780, "day_off": false, "days_before_order": 1, "is_relative": false},
        "tuesday": {"order_before": 780, "day_off": false, "days_before_order": 1, "is_relative": false},
        "wednesday": {"order_before": 900, "day_off": false, "days_before_order": 1, "is_relative": false},
        "thursday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": false},
        "friday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": false},
        "saturday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": false},
        "sunday": {"order_before": 720, "day_off": false, "days_before_order": 2, "is_relative": false}
    },
    "position": "BOTTOM",
    "help_bakeries_promo": null
}, {
    "id": 3,
    "name": "¡Norwegian Bakery!",
    "email": "igor@devhouse.pro",
    "phone": "12312312",
    "picture": 452,
    "country": {
        "code": "NO",
        "name": "Norway",
        "currency": "NOK",
        "currency_symbol": "kr",
        "phone_code": "47",
        "language": "no"
    },
    "pay_in_store_widget_enabled": false,
    "pay_in_store_widget_only_method": false,
    "pay_in_store_store_enabled": true,
    "pay_in_store_store_only_method": true,
    "home_page_url": "",
    "schedule": {
        "monday": {"order_before": 720, "day_off": false, "days_before_order": 3, "is_relative": false},
        "tuesday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "wednesday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "thursday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "friday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "saturday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": false},
        "sunday": {"order_before": 720, "day_off": false, "days_before_order": 2, "is_relative": false}
    },
    "position": "MIDDLE",
    "help_bakeries_promo": null
}, {
    "id": 10,
    "name": "Hancock bakey",
    "email": "test@test.te",
    "phone": "",
    "picture": 450,
    "country": {
        "code": "NO",
        "name": "Norway",
        "currency": "NOK",
        "currency_symbol": "kr",
        "phone_code": "47",
        "language": "no"
    },
    "pay_in_store_widget_enabled": false,
    "pay_in_store_widget_only_method": false,
    "pay_in_store_store_enabled": false,
    "pay_in_store_store_only_method": false,
    "home_page_url": "",
    "schedule": {
        "monday": {"order_before": 720, "day_off": false, "days_before_order": 3, "is_relative": false},
        "tuesday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "wednesday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "thursday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "friday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "saturday": {"order_before": null, "day_off": false, "days_before_order": 0, "is_relative": false},
        "sunday": {"order_before": null, "day_off": false, "days_before_order": 0, "is_relative": false}
    },
    "position": "MIDDLE",
    "help_bakeries_promo": ""
}, {
    "id": 4,
    "name": "bakery",
    "email": "natalia.d@devhouse.pro",
    "phone": "",
    "picture": 453,
    "country": {
        "code": "NO",
        "name": "Norway",
        "currency": "NOK",
        "currency_symbol": "kr",
        "phone_code": "47",
        "language": "no"
    },
    "pay_in_store_widget_enabled": false,
    "pay_in_store_widget_only_method": false,
    "pay_in_store_store_enabled": false,
    "pay_in_store_store_only_method": false,
    "home_page_url": "",
    "schedule": {
        "monday": {"order_before": 900, "day_off": false, "days_before_order": 0, "is_relative": false},
        "tuesday": {"order_before": 900, "day_off": false, "days_before_order": 0, "is_relative": false},
        "wednesday": {"order_before": 900, "day_off": false, "days_before_order": 0, "is_relative": false},
        "thursday": {"order_before": 900, "day_off": false, "days_before_order": 0, "is_relative": false},
        "friday": {"order_before": 900, "day_off": false, "days_before_order": 0, "is_relative": false},
        "saturday": {"order_before": null, "day_off": false, "days_before_order": 0, "is_relative": false},
        "sunday": {"order_before": null, "day_off": false, "days_before_order": 0, "is_relative": false}
    },
    "position": "MIDDLE",
    "help_bakeries_promo": null
}, {
    "id": 7,
    "name": "fork and knifeddasd",
    "email": "dsdasd@mar.ru",
    "phone": "54356666",
    "picture": 454,
    "country": {
        "code": "NO",
        "name": "Norway",
        "currency": "NOK",
        "currency_symbol": "kr",
        "phone_code": "47",
        "language": "no"
    },
    "pay_in_store_widget_enabled": false,
    "pay_in_store_widget_only_method": false,
    "pay_in_store_store_enabled": false,
    "pay_in_store_store_only_method": false,
    "home_page_url": "",
    "schedule": {
        "monday": {"order_before": 720, "day_off": false, "days_before_order": 4, "is_relative": true},
        "tuesday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "wednesday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "thursday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "friday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "saturday": {"order_before": 720, "day_off": false, "days_before_order": 3, "is_relative": true},
        "sunday": {"order_before": 720, "day_off": false, "days_before_order": 2, "is_relative": true}
    },
    "position": "MIDDLE",
    "help_bakeries_promo": null
}, {
    "id": 26,
    "name": "north bakery",
    "email": "",
    "phone": "",
    "picture": null,
    "country": {
        "code": "NO",
        "name": "Norway",
        "currency": "NOK",
        "currency_symbol": "kr",
        "phone_code": "47",
        "language": "no"
    },
    "pay_in_store_widget_enabled": false,
    "pay_in_store_widget_only_method": false,
    "pay_in_store_store_enabled": false,
    "pay_in_store_store_only_method": false,
    "home_page_url": "",
    "schedule": {
        "monday": {"order_before": 720, "day_off": false, "days_before_order": 3, "is_relative": false},
        "tuesday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "wednesday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "thursday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "friday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "saturday": {"order_before": null, "day_off": false, "days_before_order": 0, "is_relative": false},
        "sunday": {"order_before": null, "day_off": false, "days_before_order": 0, "is_relative": false}
    },
    "position": "MIDDLE",
    "help_bakeries_promo": null
}, {
    "id": 90,
    "name": "Hüla Hola Bakery",
    "email": "hola@hola.com",
    "phone": "",
    "picture": null,
    "country": {
        "code": "NO",
        "name": "Norway",
        "currency": "NOK",
        "currency_symbol": "kr",
        "phone_code": "47",
        "language": "no"
    },
    "pay_in_store_widget_enabled": false,
    "pay_in_store_widget_only_method": false,
    "pay_in_store_store_enabled": false,
    "pay_in_store_store_only_method": false,
    "home_page_url": "",
    "schedule": {
        "monday": {"order_before": 720, "day_off": false, "days_before_order": 3, "is_relative": false},
        "tuesday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "wednesday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "thursday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "friday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "saturday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "sunday": {"order_before": null, "day_off": false, "days_before_order": 0, "is_relative": false}
    },
    "position": "MIDDLE",
    "help_bakeries_promo": null
}, {
    "id": 91,
    "name": "Top Bakery",
    "email": "post@topbaker.com",
    "phone": "",
    "picture": null,
    "country": {
        "code": "NO",
        "name": "Norway",
        "currency": "NOK",
        "currency_symbol": "kr",
        "phone_code": "47",
        "language": "no"
    },
    "pay_in_store_widget_enabled": false,
    "pay_in_store_widget_only_method": false,
    "pay_in_store_store_enabled": true,
    "pay_in_store_store_only_method": true,
    "home_page_url": "",
    "schedule": {
        "monday": {"order_before": 720, "day_off": false, "days_before_order": 3, "is_relative": false},
        "tuesday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "wednesday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "thursday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "friday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "saturday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "sunday": {"order_before": null, "day_off": false, "days_before_order": 0, "is_relative": false}
    },
    "position": "MIDDLE",
    "help_bakeries_promo": null
}, {
    "id": 95,
    "name": "Test båkery1st ö",
    "email": "natalia.d+1508@devhouse.pro",
    "phone": "78789854",
    "picture": 2721,
    "country": {
        "code": "NO",
        "name": "Norway",
        "currency": "NOK",
        "currency_symbol": "kr",
        "phone_code": "47",
        "language": "no"
    },
    "pay_in_store_widget_enabled": false,
    "pay_in_store_widget_only_method": false,
    "pay_in_store_store_enabled": false,
    "pay_in_store_store_only_method": false,
    "home_page_url": "",
    "schedule": {
        "monday": {"order_before": 720, "day_off": false, "days_before_order": 3, "is_relative": false},
        "tuesday": {"order_before": 960, "day_off": false, "days_before_order": 0, "is_relative": true},
        "wednesday": {"order_before": 720, "day_off": false, "days_before_order": 0, "is_relative": true},
        "thursday": {"order_before": 720, "day_off": false, "days_before_order": 0, "is_relative": true},
        "friday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "saturday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "sunday": {"order_before": 780, "day_off": false, "days_before_order": 0, "is_relative": false}
    },
    "position": "MIDDLE",
    "help_bakeries_promo": null
}, {
    "id": 98,
    "name": "Autotest bakery!",
    "email": "autotest@devhouse.pro",
    "phone": "+79054527192",
    "picture": null,
    "country": {
        "code": "NO",
        "name": "Norway",
        "currency": "NOK",
        "currency_symbol": "kr",
        "phone_code": "47",
        "language": "no"
    },
    "pay_in_store_widget_enabled": false,
    "pay_in_store_widget_only_method": false,
    "pay_in_store_store_enabled": false,
    "pay_in_store_store_only_method": false,
    "home_page_url": "",
    "schedule": {
        "monday": {"order_before": 720, "day_off": false, "days_before_order": 3, "is_relative": false},
        "tuesday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "wednesday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "thursday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "friday": {"order_before": 720, "day_off": false, "days_before_order": 2, "is_relative": true},
        "saturday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "sunday": {"order_before": 720, "day_off": false, "days_before_order": 2, "is_relative": false}
    },
    "position": "MIDDLE",
    "help_bakeries_promo": null
}, {
    "id": 102,
    "name": "Nobel Conditori",
    "email": "natalia.d@devhouse.pro",
    "phone": "95226733",
    "picture": 1783,
    "country": {
        "code": "NO",
        "name": "Norway",
        "currency": "NOK",
        "currency_symbol": "kr",
        "phone_code": "47",
        "language": "no"
    },
    "pay_in_store_widget_enabled": false,
    "pay_in_store_widget_only_method": false,
    "pay_in_store_store_enabled": false,
    "pay_in_store_store_only_method": false,
    "home_page_url": "",
    "schedule": {
        "monday": {"order_before": 780, "day_off": false, "days_before_order": 3, "is_relative": false},
        "tuesday": {"order_before": 780, "day_off": false, "days_before_order": 1, "is_relative": true},
        "wednesday": {"order_before": 780, "day_off": false, "days_before_order": 1, "is_relative": true},
        "thursday": {"order_before": 780, "day_off": false, "days_before_order": 1, "is_relative": true},
        "friday": {"order_before": 780, "day_off": false, "days_before_order": 1, "is_relative": true},
        "saturday": {"order_before": 780, "day_off": false, "days_before_order": 1, "is_relative": true},
        "sunday": {"order_before": 780, "day_off": false, "days_before_order": 2, "is_relative": false}
    },
    "position": "MIDDLE",
    "help_bakeries_promo": null
}, {
    "id": 103,
    "name": "Baker Nordby",
    "email": "natalia.d+5@devhouse.pro",
    "phone": "+47 22222222",
    "picture": 1800,
    "country": {
        "code": "NO",
        "name": "Norway",
        "currency": "NOK",
        "currency_symbol": "kr",
        "phone_code": "47",
        "language": "no"
    },
    "pay_in_store_widget_enabled": false,
    "pay_in_store_widget_only_method": false,
    "pay_in_store_store_enabled": false,
    "pay_in_store_store_only_method": false,
    "home_page_url": "",
    "schedule": {
        "monday": {"order_before": 720, "day_off": false, "days_before_order": 3, "is_relative": false},
        "tuesday": {"order_before": 660, "day_off": false, "days_before_order": 1, "is_relative": true},
        "wednesday": {"order_before": 660, "day_off": false, "days_before_order": 1, "is_relative": true},
        "thursday": {"order_before": 660, "day_off": false, "days_before_order": 1, "is_relative": true},
        "friday": {"order_before": 660, "day_off": false, "days_before_order": 1, "is_relative": true},
        "saturday": {"order_before": 660, "day_off": false, "days_before_order": 1, "is_relative": true},
        "sunday": {"order_before": 720, "day_off": false, "days_before_order": 2, "is_relative": false}
    },
    "position": "MIDDLE",
    "help_bakeries_promo": null
}, {
    "id": 104,
    "name": "Cakes by Hancock",
    "email": "natalia.d@devhouse.pro",
    "phone": "",
    "picture": 1815,
    "country": {
        "code": "NO",
        "name": "Norway",
        "currency": "NOK",
        "currency_symbol": "kr",
        "phone_code": "47",
        "language": "no"
    },
    "pay_in_store_widget_enabled": false,
    "pay_in_store_widget_only_method": false,
    "pay_in_store_store_enabled": false,
    "pay_in_store_store_only_method": false,
    "home_page_url": "",
    "schedule": {
        "monday": {"order_before": 720, "day_off": false, "days_before_order": 3, "is_relative": false},
        "tuesday": {"order_before": 720, "day_off": false, "days_before_order": 3, "is_relative": true},
        "wednesday": {"order_before": 720, "day_off": false, "days_before_order": 3, "is_relative": true},
        "thursday": {"order_before": 720, "day_off": false, "days_before_order": 3, "is_relative": true},
        "friday": {"order_before": 720, "day_off": false, "days_before_order": 3, "is_relative": true},
        "saturday": {"order_before": 720, "day_off": false, "days_before_order": 3, "is_relative": true},
        "sunday": {"order_before": 720, "day_off": false, "days_before_order": 3, "is_relative": false}
    },
    "position": "BOTTOM",
    "help_bakeries_promo": null
}, {
    "id": 122,
    "name": "Test extra products",
    "email": "",
    "phone": "",
    "picture": 3593,
    "country": {
        "code": "NO",
        "name": "Norway",
        "currency": "NOK",
        "currency_symbol": "kr",
        "phone_code": "47",
        "language": "no"
    },
    "pay_in_store_widget_enabled": false,
    "pay_in_store_widget_only_method": false,
    "pay_in_store_store_enabled": false,
    "pay_in_store_store_only_method": false,
    "home_page_url": "",
    "schedule": {
        "monday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": false},
        "tuesday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "wednesday": {"order_before": 840, "day_off": false, "days_before_order": 2, "is_relative": true},
        "thursday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "friday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "saturday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "sunday": {"order_before": 720, "day_off": false, "days_before_order": 2, "is_relative": true}
    },
    "position": "MIDDLE",
    "help_bakeries_promo": null
}, {
    "id": 125,
    "name": "Vadim test 006",
    "email": "",
    "phone": "",
    "picture": 4923,
    "country": {
        "code": "NO",
        "name": "Norway",
        "currency": "NOK",
        "currency_symbol": "kr",
        "phone_code": "47",
        "language": "no"
    },
    "pay_in_store_widget_enabled": false,
    "pay_in_store_widget_only_method": false,
    "pay_in_store_store_enabled": false,
    "pay_in_store_store_only_method": false,
    "home_page_url": "",
    "schedule": {
        "monday": {"order_before": 960, "day_off": false, "days_before_order": 0, "is_relative": false},
        "tuesday": {"order_before": 1020, "day_off": false, "days_before_order": 5, "is_relative": true},
        "wednesday": {"order_before": 720, "day_off": false, "days_before_order": 0, "is_relative": true},
        "thursday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "friday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "saturday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "sunday": {"order_before": 720, "day_off": false, "days_before_order": 2, "is_relative": true}
    },
    "position": "MIDDLE",
    "help_bakeries_promo": null
}, {
    "id": 126,
    "name": "Test images",
    "email": "natalia.d@devhouse.pro",
    "phone": "",
    "picture": 3600,
    "country": {
        "code": "NO",
        "name": "Norway",
        "currency": "NOK",
        "currency_symbol": "kr",
        "phone_code": "47",
        "language": "no"
    },
    "pay_in_store_widget_enabled": false,
    "pay_in_store_widget_only_method": false,
    "pay_in_store_store_enabled": false,
    "pay_in_store_store_only_method": false,
    "home_page_url": "",
    "schedule": {
        "monday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": false},
        "tuesday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "wednesday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "thursday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "friday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "saturday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "sunday": {"order_before": 720, "day_off": false, "days_before_order": 2, "is_relative": true}
    },
    "position": "MIDDLE",
    "help_bakeries_promo": null
}, {
    "id": 131,
    "name": "Test widget button",
    "email": "natalia.d@devhouse.pro",
    "phone": "",
    "picture": 3656,
    "country": {
        "code": "NO",
        "name": "Norway",
        "currency": "NOK",
        "currency_symbol": "kr",
        "phone_code": "47",
        "language": "no"
    },
    "pay_in_store_widget_enabled": false,
    "pay_in_store_widget_only_method": false,
    "pay_in_store_store_enabled": false,
    "pay_in_store_store_only_method": false,
    "home_page_url": "",
    "schedule": {
        "monday": {"order_before": 720, "day_off": false, "days_before_order": 3, "is_relative": false},
        "tuesday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "wednesday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "thursday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "friday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "saturday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "sunday": {"order_before": 720, "day_off": false, "days_before_order": 2, "is_relative": true}
    },
    "position": "MIDDLE",
    "help_bakeries_promo": null
}, {
    "id": 167,
    "name": "test new widget colors",
    "email": "",
    "phone": "",
    "picture": 3786,
    "country": {
        "code": "NO",
        "name": "Norway",
        "currency": "NOK",
        "currency_symbol": "kr",
        "phone_code": "47",
        "language": "no"
    },
    "pay_in_store_widget_enabled": false,
    "pay_in_store_widget_only_method": false,
    "pay_in_store_store_enabled": false,
    "pay_in_store_store_only_method": false,
    "home_page_url": "",
    "schedule": {
        "monday": {"order_before": 720, "day_off": false, "days_before_order": 3, "is_relative": false},
        "tuesday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "wednesday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "thursday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "friday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "saturday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "sunday": {"order_before": 720, "day_off": false, "days_before_order": 2, "is_relative": true}
    },
    "position": "MIDDLE",
    "help_bakeries_promo": null
}, {
    "id": 169,
    "name": "Valaker Bakeri",
    "email": "marius@cakeiteasy.no",
    "phone": "",
    "picture": 3798,
    "country": {
        "code": "NO",
        "name": "Norway",
        "currency": "NOK",
        "currency_symbol": "kr",
        "phone_code": "47",
        "language": "no"
    },
    "pay_in_store_widget_enabled": true,
    "pay_in_store_widget_only_method": false,
    "pay_in_store_store_enabled": true,
    "pay_in_store_store_only_method": false,
    "home_page_url": "",
    "schedule": {
        "monday": {"order_before": 720, "day_off": false, "days_before_order": 3, "is_relative": false},
        "tuesday": {"order_before": 780, "day_off": false, "days_before_order": 1, "is_relative": true},
        "wednesday": {"order_before": 780, "day_off": false, "days_before_order": 1, "is_relative": true},
        "thursday": {"order_before": 780, "day_off": false, "days_before_order": 1, "is_relative": true},
        "friday": {"order_before": 780, "day_off": false, "days_before_order": 1, "is_relative": true},
        "saturday": {"order_before": 780, "day_off": false, "days_before_order": 1, "is_relative": true},
        "sunday": {"order_before": 780, "day_off": false, "days_before_order": 2, "is_relative": true}
    },
    "position": "MIDDLE",
    "help_bakeries_promo": null
}, {
    "id": 172,
    "name": "Test activate widget",
    "email": "natalia.d@devhouse.pro",
    "phone": "+47 846565",
    "picture": 3869,
    "country": {
        "code": "NO",
        "name": "Norway",
        "currency": "NOK",
        "currency_symbol": "kr",
        "phone_code": "47",
        "language": "no"
    },
    "pay_in_store_widget_enabled": true,
    "pay_in_store_widget_only_method": false,
    "pay_in_store_store_enabled": true,
    "pay_in_store_store_only_method": false,
    "home_page_url": "",
    "schedule": {
        "monday": {"order_before": 720, "day_off": false, "days_before_order": 3, "is_relative": false},
        "tuesday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "wednesday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "thursday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "friday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "saturday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "sunday": {"order_before": 720, "day_off": false, "days_before_order": 2, "is_relative": true}
    },
    "position": "MIDDLE",
    "help_bakeries_promo": null
}, {
    "id": 174,
    "name": "Test bakery",
    "email": "natalia.d@devhouse.pro",
    "phone": "",
    "picture": 3946,
    "country": {
        "code": "NO",
        "name": "Norway",
        "currency": "NOK",
        "currency_symbol": "kr",
        "phone_code": "47",
        "language": "no"
    },
    "pay_in_store_widget_enabled": true,
    "pay_in_store_widget_only_method": false,
    "pay_in_store_store_enabled": true,
    "pay_in_store_store_only_method": false,
    "home_page_url": "",
    "schedule": {
        "monday": {"order_before": 720, "day_off": false, "days_before_order": 3, "is_relative": false},
        "tuesday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "wednesday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "thursday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "friday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "saturday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "sunday": {"order_before": 720, "day_off": false, "days_before_order": 2, "is_relative": true}
    },
    "position": "MIDDLE",
    "help_bakeries_promo": null
}, {
    "id": 178,
    "name": "Test days off",
    "email": "",
    "phone": "",
    "picture": 4787,
    "country": {
        "code": "NO",
        "name": "Norway",
        "currency": "NOK",
        "currency_symbol": "kr",
        "phone_code": "47",
        "language": "no"
    },
    "pay_in_store_widget_enabled": true,
    "pay_in_store_widget_only_method": false,
    "pay_in_store_store_enabled": true,
    "pay_in_store_store_only_method": false,
    "home_page_url": "",
    "schedule": {
        "monday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": false},
        "tuesday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "wednesday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "thursday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "friday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "saturday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "sunday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true}
    },
    "position": "MIDDLE",
    "help_bakeries_promo": null
}, {
    "id": 195,
    "name": "Test printing text",
    "email": "natalia.d@devhouse.pro",
    "phone": "",
    "picture": 5238,
    "country": {
        "code": "NO",
        "name": "Norway",
        "currency": "NOK",
        "currency_symbol": "kr",
        "phone_code": "47",
        "language": "no"
    },
    "pay_in_store_widget_enabled": true,
    "pay_in_store_widget_only_method": false,
    "pay_in_store_store_enabled": true,
    "pay_in_store_store_only_method": false,
    "home_page_url": "",
    "schedule": {
        "monday": {"order_before": 720, "day_off": false, "days_before_order": 3, "is_relative": false},
        "tuesday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "wednesday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "thursday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "friday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "saturday": {"order_before": 720, "day_off": true, "days_before_order": 1, "is_relative": true},
        "sunday": {"order_before": 720, "day_off": true, "days_before_order": 2, "is_relative": true}
    },
    "position": "MIDDLE",
    "help_bakeries_promo": null
}, {
    "id": 206,
    "name": "Test payment intent",
    "email": "natalia.d@devhouse.pro",
    "phone": "",
    "picture": 5683,
    "country": {
        "code": "NO",
        "name": "Norway",
        "currency": "NOK",
        "currency_symbol": "kr",
        "phone_code": "47",
        "language": "no"
    },
    "pay_in_store_widget_enabled": false,
    "pay_in_store_widget_only_method": false,
    "pay_in_store_store_enabled": false,
    "pay_in_store_store_only_method": false,
    "home_page_url": "",
    "schedule": {
        "monday": {"order_before": 720, "day_off": false, "days_before_order": 3, "is_relative": false},
        "tuesday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "wednesday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "thursday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "friday": {"order_before": 720, "day_off": false, "days_before_order": 1, "is_relative": true},
        "saturday": {"order_before": 720, "day_off": true, "days_before_order": 1, "is_relative": true},
        "sunday": {"order_before": 720, "day_off": true, "days_before_order": 2, "is_relative": true}
    },
    "position": "MIDDLE",
    "help_bakeries_promo": null
}]